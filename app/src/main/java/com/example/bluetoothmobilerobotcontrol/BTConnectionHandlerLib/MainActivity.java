package com.example.bluetoothmobilerobotcontrol.BTConnectionHandlerLib;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.bluetoothmobilerobotcontrol.BTConnectionHandlerLib.exceptions.BTHandlingException;
import com.example.bluetoothmobilerobotcontrol.R;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BTConnectionHandler bt = Singleton.getInstance();

        final Button sendButton = (Button) findViewById(R.id.sendbutton);
        final Button joyButton = (Button) findViewById(R.id.joystick);
        final EditText text = findViewById(R.id.editText);
        final EditText text2 = findViewById(R.id.editText2);
        final Button connectButton = (Button) findViewById(R.id.connectbutton);
        final Button discButton = (Button) findViewById(R.id.discButton);
        final Button frontButton = (Button) findViewById(R.id.button);
        final Button rightButton = (Button) findViewById(R.id.button7);
        final Button leftButton = (Button) findViewById(R.id.button6);
        final Button backButton = (Button) findViewById(R.id.button5);
        final Button stopButton = (Button) findViewById(R.id.button8);

        joyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startJoystickActivity();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.sendData(text.getText().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                }
            }
        });
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.connectToBTDevice(text2.getText().toString());
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        discButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bt.closeConnection();
            }
        });
        frontButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.sendData("A");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                }
            }
        });
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.sendData("R");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                }

            }
        });
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.sendData("L");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                }
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.sendData("B");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                }
            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    bt.sendData("S");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BTHandlingException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private void startJoystickActivity()
    {
        this.startActivity( new Intent(this, Joystick.class));
    }
}
