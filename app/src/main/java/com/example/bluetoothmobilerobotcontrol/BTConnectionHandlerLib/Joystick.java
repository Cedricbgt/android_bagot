package com.example.bluetoothmobilerobotcontrol.BTConnectionHandlerLib;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.example.bluetoothmobilerobotcontrol.BTConnectionHandlerLib.JoystickView;
import com.example.bluetoothmobilerobotcontrol.R;

public class Joystick extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick);
        LinearLayout lay = (LinearLayout) findViewById(R.id.layout);
        JoystickView joyview = new JoystickView(this);
        lay.addView(joyview);
        JoystickView.ValueChangedHandler value = new JoystickView.ValueChangedHandler() {
            @Override
            public void onValueChanged(int Vg, int Vd) {
                try {
                    Singleton.getInstance().sendData("Vg:"+"VD:");
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        joyview.setValueChangeHandler(value);
    }

}
