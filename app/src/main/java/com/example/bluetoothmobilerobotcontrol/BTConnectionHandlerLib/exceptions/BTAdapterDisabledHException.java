package com.example.bluetoothmobilerobotcontrol.BTConnectionHandlerLib.exceptions;

/**
 * Created by raievskc on 2/14/16.
 */
public class BTAdapterDisabledHException extends BTHandlingException {

   public BTAdapterDisabledHException() {
      super("Bluetooth Adapter disabled");
   }
}
