package com.example.bluetoothmobilerobotcontrol.BTConnectionHandlerLib;

public class Singleton {
    private static BTConnectionHandler s = new BTConnectionHandler();

    public static BTConnectionHandler getInstance() {
        return s;
    }

    private Singleton() {
    }
}
